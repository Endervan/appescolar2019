import {Component} from '@angular/core';
import {AlertController, LoadingController, NavController} from 'ionic-angular';
import {Http} from '@angular/http';
import {Usuario} from '../../domain/usuario/usuario';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HomePage} from '../home/home';


@Component({
    selector: 'page-usuario-cadastro',
    templateUrl: 'usuario-cadastro.html',
})
export class UsuarioCadastroPage {
    public data;
    public http;
    public usuario: Usuario;
    Cadastro: FormGroup;
    nome: AbstractControl;
    email: AbstractControl;
    password: AbstractControl;
    telefone: AbstractControl;

    constructor(public navCtrl: NavController,
                http: Http,
                public formBuilder: FormBuilder,
                private _alertCtrl: AlertController,
                private _loadingCtrl: LoadingController) {
        this.Cadastro = formBuilder.group({
            nome: ['', Validators.compose([Validators.maxLength(30), Validators.required])],
            email: ['',
                Validators.compose(
                    [Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/), Validators.required])],
            password: ['',
                Validators.compose(
            [Validators.minLength(3), /*Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$'),*/Validators.required])],
            telefone: ['',Validators.compose(
                [Validators.minLength(14), Validators.required])],
        });
        this.nome = this.Cadastro.controls['nome']
        this.email = this.Cadastro.controls['email']
        this.password = this.Cadastro.controls['password']
        this.telefone = this.Cadastro.controls['telefone']

        this.data = {};
        this.data.response = '';
        this.http = http;
        this.usuario = new Usuario(null, null, null, null, null, null);

    }

    Cadastrar() {
        var link = 'http://localhost/escolar/usuario/cadastrar_usuario_ionic';
        var data = JSON.stringify({
            nome: this.usuario.nome,
            email: this.usuario.email,
            telefone: this.usuario.telefone,
            password: this.usuario.password
        });

        const loader = this._loadingCtrl.create({
            content: "Processando, por favor aguarde.",
        });
        loader.present();
        console.log(data);

        loader.dismiss();


        // Iniciando a conexão HTTP para cadastro via JSON
        this.http.post(link, data)
            .subscribe(data => {
                this.data.response = data._body;
                var res = this.data.response.split("|");

                console.log(this.data.response)
                if (res[0] == "sucesso") {
                    let loader = this._loadingCtrl.create({
                        content: ' Cadastro Efetuado com sucesso '
                    });
                    loader.present();

                    loader.dismiss();

                    this.navCtrl.push(HomePage);
                } else {
                    console.log(res[0])

                    if (res[0]==1){
                        this._alertCtrl
                            .create({
                                title: 'Atenção ',
                                buttons: [{text: 'OK '}],
                                subTitle: " Esse email já está sendo utilizado."
                            }).present();

                        loader.dismiss();

                    }else if(res[0]==0){
                        this._alertCtrl
                            .create({
                                title: 'Atenção',
                                buttons: [{text: 'OK '}],
                                subTitle: " Esse telefone já está sendo utilizado."
                            }).present();

                        loader.dismiss();

                    }

                }
            }, error => {
                console.log("Ocorreu algum erro!");
                this._alertCtrl
                    .create({
                        title: 'Sem Conexão',
                        buttons: [{text: 'OK estou ciente!'}],
                        subTitle: " Tente em Alguns instantes"
                    }).present();
            });
        //}
    }
}
