var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
var EscolaProvider = (function () {
    function EscolaProvider(http, storage) {
        var _this = this;
        this.http = http;
        this.storage = storage;
        this.url = 'http://localhost/escolar/escola/';
        this.storage.get('idusuario')
            .then(function (idusuario) {
            idusuario;
            var id_usuario = idusuario;
            console.log('recebendo = ' + _this.id_usuario);
        });
    }
    //    busca os dados
    EscolaProvider.prototype.getDados = function () {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get(this.url + 'getDados/', { headers: headers }).toPromise();
    };
    //    busca os dados com id
    EscolaProvider.prototype.getDadosId = function () {
        console.log(this.url + ("getDados/" + this.id_usuario));
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get(this.url + ("getDados/" + this.id_usuario), { headers: headers }).toPromise();
    };
    //    cadastro
    EscolaProvider.prototype.postData = function (parans) {
        console.log('URL = ' + this.url);
        var headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this.http.post(this.url + "cadastra", parans, {
            headers: headers,
            method: "POST"
        }).map(function (res) {
            return res.json();
        });
    };
    EscolaProvider.prototype.deleteData = function (id) {
        var headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this.http.post(this.url + "deletar", id, {
            headers: headers
        }).map(function (res) {
            return res.json();
        });
    };
    EscolaProvider.prototype.updateData = function (data) {
        var headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this.http.post(this.url + "Update", data, {
            headers: headers,
            method: "POST"
        }).map(function (res) {
            return res.json();
        });
    };
    EscolaProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http, Storage])
    ], EscolaProvider);
    return EscolaProvider;
}());
export { EscolaProvider };
//# sourceMappingURL=escola.js.map