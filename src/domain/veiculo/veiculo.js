var Veiculo = (function () {
    function Veiculo(id, placa, marca, ano, modelo, ativo) {
        this.id = id;
        this.placa = placa;
        this.marca = marca;
        this.ano = ano;
        this.modelo = modelo;
        this.ativo = ativo;
    }
    return Veiculo;
}());
export { Veiculo };
//# sourceMappingURL=veiculo.js.map