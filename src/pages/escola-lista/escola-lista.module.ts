import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EscolaListaPage } from './escola-lista';

@NgModule({
  declarations: [
    EscolaListaPage,
  ],
  imports: [
    IonicPageModule.forChild(EscolaListaPage),
  ],
})
export class EscolaListaPageModule {}
