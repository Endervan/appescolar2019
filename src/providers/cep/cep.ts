import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';




@Injectable()
export class CepProvider {

  url : string = 'http://localhost/Escolar/escola/';

  constructor(public http: Http) {
    console.log('Hello CepProvider Provider');
  }


  getDados(){
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    return this.http.get(this.url  + 'getDados', { headers: headers }).toPromise();

  }

  
}



