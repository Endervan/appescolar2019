var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { Veiculo } from '../../domain/veiculo/veiculo';
import { FormBuilder, Validators } from "@angular/forms";
import { Http } from '@angular/http';
var VeiculoCadastroPage = (function () {
    function VeiculoCadastroPage(navCtrl, http, formBuilder, _alertCtrl, _loadingCtrl) {
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this._alertCtrl = _alertCtrl;
        this._loadingCtrl = _loadingCtrl;
        //  validador dos campos
        this.Cadastro_veiculo = formBuilder.group({
            placa: ['', Validators.compose([Validators.pattern(/[A-Za-z]{3}-[0-9]{4}/), Validators.required])],
            marca: ['', Validators.compose([Validators.required])],
        });
        //  verifica a validacao, se estiver ok libera o botão de cadastro
        this.placa = this.Cadastro_veiculo.controls['placa'];
        this.marca = this.Cadastro_veiculo.controls['marca'];
        //  geracao do JSON
        this.data = {};
        this.data.response = '';
        this.http = http;
        this.veiculo = new Veiculo(null, null, null, null, null, null);
    }
    VeiculoCadastroPage.prototype.Cadastrar = function () {
        var _this = this;
        //  link da acao
        var link = 'http://localhost/escolar/veiculo/valida_cadastro';
        //  variaveis
        var data = JSON.stringify({
            placa: this.veiculo.placa,
        });
        //  mensagem de loading
        var loader = this._loadingCtrl.create({
            content: "Processando, por favor aguarde.",
        });
        loader.present();
        console.log(data);
        // Iniciando a conexão HTTP para cadastro via JSON
        this.http.post(link, data).subscribe(function (data) {
            //var resposta = this.data.response = data._body;
            //  this.data.response = data._body;
            /* console.log(resposta);
            // console.log('TITLE = '+resposta.title);*/
            //  msg retornada
            _this._alertCtrl.create({
                title: "Aviso",
                buttons: [{ text: "OK" }],
                subTitle: "Cadastro efetuado com sucesso.   "
            }).present();
            //  fechar mensagem de loading
            loader.dismiss();
        }, function (error) {
            console.log("Ocorreu algum erro!");
            //  fechar mensagem de loading
            loader.dismiss();
            _this._alertCtrl
                .create({
                title: 'Sem Conexão',
                buttons: [{ text: 'OK estou ciente!' }],
                subTitle: " Tente em Alguns instantes"
            }).present();
        });
        /*
        this.http.post(link, data)
          .subscribe(data => {
              this.data.response = data;
              
              
               
                      this._alertCtrl
                          .create({
                              title: 'Atenção ',
                              buttons: [{text: 'OK '}],
                              subTitle: "AQUI FICA A MSG DE RETORNO"
                          }).present();
                
    
              
          }, error => {
              console.log("Ocorreu algum erro!");
              this._alertCtrl
                  .create({
                      title: 'Sem Conexão',
                      buttons: [{text: 'OK'}],
                      subTitle: " Tente em Alguns instantes"
                  }).present();
          });
          */
    };
    VeiculoCadastroPage = __decorate([
        Component({
            selector: 'page-veiculo-cadastro',
            templateUrl: 'veiculo-cadastro.html',
        }),
        __metadata("design:paramtypes", [NavController, Http, FormBuilder, AlertController, LoadingController])
    ], VeiculoCadastroPage);
    return VeiculoCadastroPage;
}());
export { VeiculoCadastroPage };
//# sourceMappingURL=veiculo-cadastro.js.map