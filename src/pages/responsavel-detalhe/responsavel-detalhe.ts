import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';



@Component({
  selector: 'page-responsavel-detalhe',
  templateUrl: 'responsavel-detalhe.html',
})
export class ResponsavelDetalhePage {
    dado:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
      //pegando dados por params
      this.dado = this.navParams.get('detalhesResponsavel');
  }



}
