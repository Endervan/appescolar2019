export class Responsavel {

    public $key: string;

    constructor(
        public idresponsavel: string,
        public nome: string,
        public cpf: string,
        public complemento: string,
        public id_usuario: string,
        public telefone_celular: string,
        public telefone_residencial: string,
        public telefone_trabalho: string,
        public uf_id: string,
        public uf_titulo: string,
        public cidade_titulo: string,
        public bairro_titulo: string,
        public usuario_id: string,
        public usuario_nome: string
    ) {}

}
