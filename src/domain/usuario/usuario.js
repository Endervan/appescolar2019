var Usuario = (function () {
    function Usuario(id, nome, email, password, telefone, status) {
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.password = password;
        this.telefone = telefone;
        this.status = status;
    }
    return Usuario;
}());
export { Usuario };
//# sourceMappingURL=usuario.js.map