import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EscolaDetalhePage } from './escola-detalhe';

@NgModule({
  declarations: [
    EscolaDetalhePage,
  ],
  imports: [
    IonicPageModule.forChild(EscolaDetalhePage),
  ],
})
export class EscolaDetalhePageModule {}
