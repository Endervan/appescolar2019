import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VeiculoDadosPage } from './veiculo-dados';

@NgModule({
  declarations: [
    VeiculoDadosPage,
  ],
  imports: [
    IonicPageModule.forChild(VeiculoDadosPage),
  ],
})
export class VeiculoDadosPageModule {}
