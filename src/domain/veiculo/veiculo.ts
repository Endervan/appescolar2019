export class Veiculo{

    constructor(
        public id: string,
        public placa: string,
        public marca: string,
        public ano: string,
        public modelo: string,
        public ativo: string
    ){}
}