var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Storage } from '@ionic/storage';
import { FormBuilder, Validators } from '@angular/forms';
import { EscolaProvider } from './../../providers/escola/escola';
import { Component } from '@angular/core';
import { IonicPage, AlertController, NavController, LoadingController } from 'ionic-angular';
import { EscolaListaPage } from '../escola-lista/escola-lista';
var EscolaCadastroPage = (function () {
    function EscolaCadastroPage(storage, navCtrl, formBuilder, service, alertCtrl, loadingCtrl) {
        // this.id_usuario = this.storage.get('idusuario');
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.form = {};
        //  campos do fomulario
        this.form = this.formBuilder.group({
            nome: ['', Validators.compose([Validators.required])],
            endereco: ['', Validators.required],
            complemento: [''],
            telefone: [''],
            id_usuario: +this.id_usuario
        });
    }
    //  cadastra no banco
    EscolaCadastroPage.prototype.postDados = function () {
        var _this = this;
        //  inicio loading
        var loader = this.loadingCtrl.create({
            content: "Processando, por favor aguarde.",
        });
        loader.present();
        //  envia o post para cadastro
        this.service.postData(this.form.value)
            .subscribe(function (data) {
            console.log(_this.form.value);
            loader.dismiss(); //  oculta loading
            _this.form.reset(); //  limpa formulario
            // set novamente id_usuario
            _this.id_usuario = _this.storage.get('idusuario');
            _this.ionViewDidEnter();
            var confirm = _this.alertCtrl.create({
                title: 'Atenção',
                message: 'Escola cadastra com sucesso. Deseja inserir uma nova escola?',
                buttons: [
                    {
                        text: 'Sim',
                        handler: function () {
                        }
                    },
                    {
                        text: 'Não',
                        handler: function () {
                            _this.navCtrl.push(EscolaListaPage);
                        }
                    }
                ]
            });
            confirm.present();
            // this.showAlert('Aviso', data.mensage, 'OK');  //  msg de sucesso
        }, function (err) {
            loader.dismiss(); //  oculta loading
            _this.showAlert('Atenção', err.mensage, 'OK'); //  msg de erro
            console.log(err);
        });
    };
    //  alert
    EscolaCadastroPage.prototype.showAlert = function (msgTitle, msgSubTitle, msgButtons) {
        var alert = this.alertCtrl.create({
            title: msgTitle,
            subTitle: msgSubTitle,
            buttons: [msgButtons]
        });
        alert.present();
    };
    // retorna value id_usuario storage
    EscolaCadastroPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        return this.storage.get('idusuario').then(function (val) {
            return _this.id_usuario = val;
        });
    };
    EscolaCadastroPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-escola-cadastro',
            templateUrl: 'escola-cadastro.html',
        }),
        __metadata("design:paramtypes", [Storage, NavController, FormBuilder, EscolaProvider, AlertController, LoadingController])
    ], EscolaCadastroPage);
    return EscolaCadastroPage;
}());
export { EscolaCadastroPage };
//# sourceMappingURL=escola-cadastro.js.map