import { Injectable } from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';


@Injectable()
export class AlunosProvider {

    url: string = 'http://localhost/escolar/alunos/';

  constructor(public http: Http) {
  }

    //    busca os dados
    getDados() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get(this.url + 'getDados', {headers: headers}).toPromise();
    }


    //    cadastro
    postData(parans) {

        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        return this.http.post(this.url + "cadastra", parans, {
            headers: headers,
            method: "POST"
        }).map(
            (res: Response) => {
                return res.json();
            }
        );
    }



}
