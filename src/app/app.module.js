var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { HttpModule } from '@angular/http';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { UsuarioCadastroPage } from "../pages/usuario-cadastro/usuario-cadastro";
import { UsuarioLoginPage } from "../pages/usuario-login/usuario-login";
import { UsuarioEsqueceuSenhaPage } from "../pages/usuario-esqueceu-senha/usuario-esqueceu-senha";
import { VeiculoCadastroPage } from '../pages/veiculo-cadastro/veiculo-cadastro';
import { UsuarioAlteraDadosPage } from "../pages/usuario-altera-dados/usuario-altera-dados";
import { VeiculoListaPage } from '../pages/veiculo-lista/veiculo-lista';
import { VeiculoProvider } from '../providers/veiculo/veiculo';
import { UsuarioContaPage } from "../pages/usuario-conta/usuario-conta";
import { EscolaProvider } from '../providers/escola/escola';
import { EscolaCadastroPage } from './../pages/escola-cadastro/escola-cadastro';
import { EscolaListaPage } from '../pages/escola-lista/escola-lista';
import { CepProvider } from '../providers/cep/cep';
import { FormsModule } from '@angular/forms';
import { AlunosPage } from '../pages/alunos/alunos';
import { AlunosProvider } from '../providers/alunos/alunos';
import { ResponsavelProvider } from '../providers/responsavel/responsavel';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { EscolaDetalhePage } from '../pages/escola-detalhe/escola-detalhe';
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                MyApp,
                HomePage,
                UsuarioCadastroPage,
                UsuarioLoginPage,
                UsuarioEsqueceuSenhaPage,
                VeiculoCadastroPage,
                UsuarioAlteraDadosPage,
                VeiculoListaPage,
                UsuarioContaPage,
                EscolaCadastroPage,
                EscolaListaPage,
                AlunosPage,
                EscolaDetalhePage
            ],
            imports: [
                BrowserModule,
                HttpModule,
                IonicModule.forRoot(MyApp),
                IonicStorageModule.forRoot(),
                BrMaskerModule,
                FormsModule,
                SelectSearchableModule,
            ],
            bootstrap: [IonicApp],
            entryComponents: [
                MyApp,
                HomePage,
                UsuarioCadastroPage,
                UsuarioLoginPage,
                UsuarioEsqueceuSenhaPage,
                VeiculoCadastroPage,
                UsuarioAlteraDadosPage,
                VeiculoListaPage,
                UsuarioContaPage,
                EscolaCadastroPage,
                EscolaListaPage,
                AlunosPage,
                EscolaDetalhePage
            ],
            providers: [
                StatusBar,
                SplashScreen,
                { provide: ErrorHandler, useClass: IonicErrorHandler },
                VeiculoProvider,
                EscolaProvider,
                CepProvider,
                AlunosProvider,
                ResponsavelProvider,
            ]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map