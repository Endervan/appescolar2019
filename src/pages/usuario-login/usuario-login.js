var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, ToastController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Usuario } from '../../domain/usuario/usuario';
import { FormBuilder, Validators } from "@angular/forms";
import { UsuarioCadastroPage } from "../usuario-cadastro/usuario-cadastro";
import { UsuarioEsqueceuSenhaPage } from "../usuario-esqueceu-senha/usuario-esqueceu-senha";
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
var UsuarioLoginPage = (function () {
    function UsuarioLoginPage(navCtrl, http, formBuilder, _alertCtrl, _loadingCtrl, storage, _toastCtrl) {
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this._alertCtrl = _alertCtrl;
        this._loadingCtrl = _loadingCtrl;
        this.storage = storage;
        this._toastCtrl = _toastCtrl;
        this.Logando = formBuilder.group({
            email: ['',
                Validators.compose([Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/), Validators.required])],
            password: ['', Validators.required],
        });
        this.email = this.Logando.controls['email'];
        this.password = this.Logando.controls['password'];
        this.data = {};
        this.data.response = '';
        this.http = http;
        this.usuario = new Usuario(null, null, null, null, null, null);
    }
    //buscando BD os dados pra login
    UsuarioLoginPage.prototype.Login = function () {
        var _this = this;
        var link = 'http://localhost/escolar/usuario/login_ionic';
        var data = JSON.stringify({ email: this.usuario.email, password: this.usuario.password });
        var loader = this._loadingCtrl.create({
            content: "Processando, por favor aguarde.",
        });
        loader.present();
        this.http.post(link, data)
            .subscribe(function (data) {
            _this.data.response = data._body;
            var res = _this.data.response.split("|");
            console.log(_this.data.response);
            if (res[1] == "sucesso") {
                sessionStorage.setItem("usuarioId", res[0]);
                sessionStorage.setItem("usuarioLogado", _this.usuario.email);
                sessionStorage.setItem("logado", "sim");
                sessionStorage.setItem("token", res[3]);
                //  testando o IonicStorageModule 
                _this.storage.set('idusuario', res[0]);
                _this.storage.set('usuarioLogado', _this.usuario.email);
                _this.storage.set('logado', 'sim');
                _this.storage.set('token', res[3]);
                loader.dismiss();
                _this.navCtrl.setRoot(HomePage);
            }
            else {
                var toast = _this._toastCtrl.create({
                    message: 'Email ou Senha Invalídos',
                    duration: 3000,
                    position: 'top'
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                });
                toast.present();
                loader.dismiss();
            }
        }, function (error) {
            console.log("Ocorreu algum erro!");
            _this._alertCtrl
                .create({
                title: 'Sem Conexão',
                buttons: [{ text: 'OK estou ciente!' }],
                subTitle: "Tente Mais Tarde."
            }).present();
        });
        loader.dismiss();
    };
    UsuarioLoginPage.prototype.IrCadastro = function () {
        this.navCtrl.push(UsuarioCadastroPage);
    };
    UsuarioLoginPage.prototype.EsqueceuSenha = function () {
        this.navCtrl.push(UsuarioEsqueceuSenhaPage);
    };
    UsuarioLoginPage = __decorate([
        Component({
            selector: 'page-usuario-login',
            templateUrl: 'usuario-login.html',
        }),
        __metadata("design:paramtypes", [NavController,
            Http,
            FormBuilder,
            AlertController,
            LoadingController,
            Storage,
            ToastController])
    ], UsuarioLoginPage);
    return UsuarioLoginPage;
}());
export { UsuarioLoginPage };
//# sourceMappingURL=usuario-login.js.map