import { Component } from '@angular/core';
import { ToastController, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VeiculoProvider } from '../../providers/veiculo/veiculo';



@Component({
  selector: 'page-veiculo-lista',
  templateUrl: 'veiculo-lista.html',
})
export class VeiculoListaPage {

  cepDigitado : number;

  public form: FormGroup;
  response: any;
  constructor(private veiculoProvider: VeiculoProvider, private fb: FormBuilder, private toastCtrl : ToastController, public loadingCtrl: LoadingController) {
    this.form = this.fb.group({
      cep: ['', Validators.compose([
        Validators.required
      ])],
    });

  }


  ionViewDidLoad() {

  }
 
  consultarEnderecoPeloCep() {
    let aguarde = this.loadingCtrl.create({
      content: "Processando"
    });

    aguarde.present(); 
    
    let valueCep = this.form.controls['cep'].value;
    this.veiculoProvider.listarEndereco(valueCep).then((data) => {
      this.response = data.json(); 

      console.log(this.response);

      aguarde.dismiss();

    }).catch((response) => {
      aguarde.dismiss();
      this.toastCtrl.create({
        message : 'Cep não encontrado!',
        duration: 2000,
        position : 'top'
      }).present();
    });
  }

  
  

}
