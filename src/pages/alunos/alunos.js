var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, IonicPage, LoadingController, NavController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { AlunosProvider } from '../../providers/alunos/alunos';
import { HomePage } from '../home/home';
import { ResponsavelProvider } from '../../providers/responsavel/responsavel';
import { EscolaProvider } from '../../providers/escola/escola';
var AlunosPage = (function () {
    function AlunosPage(navCtrl, formBuilder, alertCtrl, loadingCtrl, alunosProvider, responsavelProvider, escolaProvider) {
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alunosProvider = alunosProvider;
        this.responsavelProvider = responsavelProvider;
        this.escolaProvider = escolaProvider;
        //  campos do fomulario
        this.form = this.formBuilder.group({
            nome: ['', Validators.compose([Validators.required])],
            sala: ['', Validators.compose([Validators.minLength(2), Validators.required])],
            sexo: ['', Validators.required],
            turno: ['', Validators.required],
            id_responsavel: ['', Validators.required],
            id_escola: ['', Validators.required],
        });
        // populando dados do select (tabela Responsavel)
        this.getDados();
        this.dadosSelect = this.dados;
        // populando dados do select (tabela escola)
        this.getDadosEscolar();
        this.dadosSelectEscola = this.dadosEscolar;
    }
    //  cadastra no banco
    AlunosPage.prototype.postDados = function () {
        var _this = this;
        //  inicio loading
        var loader = this.loadingCtrl.create({
            content: "Processando, por favor aguarde.",
        });
        loader.present();
        //  envia o post para cadastro
        if (this.form != null) {
            this.alunosProvider.postData(this.form.value)
                .subscribe(function (data) {
                loader.dismiss(); //  oculta loading
                _this.form.reset(); //  limpa formulario
                var confirm = _this.alertCtrl.create({
                    title: 'Atenção',
                    message: 'Aluno cadastra com sucesso. Deseja inserir uma nova registro?',
                    buttons: [
                        {
                            text: 'Sim',
                            handler: function () {
                            }
                        },
                        {
                            text: 'Não',
                            handler: function () {
                                _this.navCtrl.push(HomePage);
                            }
                        }
                    ]
                });
                confirm.present();
                // this.showAlert('Aviso', data.mensage, 'OK');  //  msg de sucesso
            }, function (err) {
                loader.dismiss(); //  oculta loading
                _this.showAlert('Atenção', err.mensage, 'OK'); //  msg de erro
                console.log(err);
            });
        }
    };
    //buscando dados tabela responsavel
    AlunosPage.prototype.getDados = function () {
        var _this = this;
        //  buscando os dados
        this.responsavelProvider.getDados().then(function (data) {
            //  armazena o json na variavel
            _this.dados = data.json();
            // console.log(this.dados);
        }).catch(function (response) {
            console.log(response);
        });
    };
    //buscando dados tabela escola
    AlunosPage.prototype.getDadosEscolar = function () {
        var _this = this;
        //  buscando os dados
        this.escolaProvider.getDados().then(function (data) {
            //  armazena o json na variavel
            _this.dadosEscolar = data.json();
            // console.log(this.dados);
        }).catch(function (response) {
            console.log(response);
        });
    };
    //  alert
    AlunosPage.prototype.showAlert = function (msgTitle, msgSubTitle, msgButtons) {
        var alert = this.alertCtrl.create({
            title: msgTitle,
            subTitle: msgSubTitle,
            buttons: [msgButtons]
        });
        alert.present();
    };
    // filtra conteudo Select
    AlunosPage.prototype.portChange = function (event) {
        console.log('array select:', event.value);
    };
    AlunosPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-alunos',
            templateUrl: 'alunos.html',
        }),
        __metadata("design:paramtypes", [NavController,
            FormBuilder,
            AlertController,
            LoadingController,
            AlunosProvider,
            ResponsavelProvider,
            EscolaProvider])
    ], AlunosPage);
    return AlunosPage;
}());
export { AlunosPage };
//# sourceMappingURL=alunos.js.map