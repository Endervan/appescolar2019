var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Usuario } from '../../domain/usuario/usuario';
import { FormBuilder, Validators } from "@angular/forms";
var UsuarioEsqueceuSenhaPage = (function () {
    function UsuarioEsqueceuSenhaPage(navCtrl, http, formBuilder, _loadingCtrl, _alertCtrl) {
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this._loadingCtrl = _loadingCtrl;
        this._alertCtrl = _alertCtrl;
        this.Recupera = formBuilder.group({
            email: ['',
                Validators.compose([Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/), Validators.required])],
        });
        this.email = this.Recupera.controls['email'];
        this.data = {};
        this.data.response = '';
        this.http = http;
        this.usuario = new Usuario(null, null, null, null, null, null);
    }
    UsuarioEsqueceuSenhaPage.prototype.Recupera_senha = function () {
        var _this = this;
        var link = 'http://localhost/escolar/usuario/Recupera_senha';
        var data = JSON.stringify({
            email: this.usuario.email
        });
        var loader = this._loadingCtrl.create({
            content: "Processando, por favor aguarde.",
        });
        loader.present();
        // Iniciando a conexão HTTP para cadastro via JSON
        this.http.post(link, data)
            .subscribe(function (data) {
            _this.data.response = data._body;
            var res = _this.data.response.split("|");
            console.log(_this.data.response);
            if (res[1] == "sucesso") {
                _this._alertCtrl
                    .create({
                    title: 'Atenção',
                    buttons: [{ text: 'OK ' }],
                    subTitle: " Sua nova senha foi enviada para seu email."
                }).present();
                loader.dismiss();
            }
            else {
                _this._alertCtrl
                    .create({
                    title: 'Atenção',
                    buttons: [{ text: 'OK ' }],
                    subTitle: " Email Não Cadastrado."
                }).present();
                loader.dismiss();
            }
        }, function (error) {
            _this._alertCtrl
                .create({
                title: 'Sem Conexão',
                buttons: [{ text: 'OK estou ciente!' }],
                subTitle: " Tente em Alguns instantes"
            }).present();
        });
        //}
    };
    UsuarioEsqueceuSenhaPage = __decorate([
        Component({
            selector: 'page-usuario-esqueceu-senha',
            templateUrl: 'usuario-esqueceu-senha.html',
        }),
        __metadata("design:paramtypes", [NavController,
            Http,
            FormBuilder,
            LoadingController,
            AlertController])
    ], UsuarioEsqueceuSenhaPage);
    return UsuarioEsqueceuSenhaPage;
}());
export { UsuarioEsqueceuSenhaPage };
//# sourceMappingURL=usuario-esqueceu-senha.js.map