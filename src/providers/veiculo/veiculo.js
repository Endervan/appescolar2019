var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
var VeiculoProvider = (function () {
    function VeiculoProvider(http) {
        this.http = http;
    }
    VeiculoProvider.prototype.listarEndereco = function (cep) {
        //  let url = "https://viacep.com.br/ws/" + cep + "/json/";
        var url = "http://localhost/Escolar/veiculo/get_dados/1";
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(url, { headers: headers }).toPromise();
    };
    VeiculoProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http])
    ], VeiculoProvider);
    return VeiculoProvider;
}());
export { VeiculoProvider };
//# sourceMappingURL=veiculo.js.map