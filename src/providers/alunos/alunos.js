var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
var AlunosProvider = (function () {
    function AlunosProvider(http) {
        this.http = http;
        this.url = 'http://localhost/escolar/alunos/';
    }
    //    busca os dados
    AlunosProvider.prototype.getDados = function () {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get(this.url + 'getDados', { headers: headers }).toPromise();
    };
    //    cadastro
    AlunosProvider.prototype.postData = function (parans) {
        var headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this.http.post(this.url + "cadastra", parans, {
            headers: headers,
            method: "POST"
        }).map(function (res) {
            return res.json();
        });
    };
    AlunosProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http])
    ], AlunosProvider);
    return AlunosProvider;
}());
export { AlunosProvider };
//# sourceMappingURL=alunos.js.map