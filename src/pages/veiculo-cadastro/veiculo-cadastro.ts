import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { Veiculo } from '../../domain/veiculo/veiculo';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Http } from '@angular/http';





@Component({
  selector: 'page-veiculo-cadastro',
  templateUrl: 'veiculo-cadastro.html',
})
export class VeiculoCadastroPage {

  public http: Http;
  public data;
  public Cadastro_veiculo: FormGroup;
  
  public veiculo: Veiculo;
  public placa: AbstractControl;
  public marca: AbstractControl;
  

  constructor(public navCtrl: NavController, http: Http , public formBuilder: FormBuilder, private _alertCtrl: AlertController, private _loadingCtrl: LoadingController){
    
    //  validador dos campos
    this.Cadastro_veiculo = formBuilder.group({
      placa: ['', Validators.compose( [ Validators.pattern(/[A-Za-z]{3}-[0-9]{4}/), Validators.required ])],
      marca: ['', Validators.compose( [ Validators.required ] )],
    });
 
   
    //  verifica a validacao, se estiver ok libera o botão de cadastro
    this.placa = this.Cadastro_veiculo.controls['placa'];
    this.marca = this.Cadastro_veiculo.controls['marca'];
    
    

    //  geracao do JSON
    this.data = {};
    this.data.response = '';
    this.http = http;
    this.veiculo = new Veiculo(null, null, null, null, null, null);

  }


 
  Cadastrar() {
    
    //  link da acao
    var link = 'http://localhost/escolar/veiculo/valida_cadastro';

    //  variaveis
    var data = JSON.stringify({
        placa: this.veiculo.placa,
    });
      
    //  mensagem de loading
    const loader = this._loadingCtrl.create({
        content: "Processando, por favor aguarde.",
    });
    loader.present();
    console.log(data);

    
    // Iniciando a conexão HTTP para cadastro via JSON
    this.http.post(link, data).subscribe(data => { 

        //var resposta = this.data.response = data._body;
      //  this.data.response = data._body;

       /* console.log(resposta);
       // console.log('TITLE = '+resposta.title);*/

        //  msg retornada
        this._alertCtrl.create({
                title: "Aviso",
                buttons: [{text: "OK"  }],
                subTitle: "Cadastro efetuado com sucesso.   "
            }).present();


        //  fechar mensagem de loading
        loader.dismiss();
                
    }, error => {
        console.log("Ocorreu algum erro!");

        //  fechar mensagem de loading
        loader.dismiss();

        this._alertCtrl
            .create({
                title: 'Sem Conexão',
                buttons: [{text: 'OK estou ciente!'}],
                subTitle: " Tente em Alguns instantes"
            }).present();
    });


    


    /*
    this.http.post(link, data) 
      .subscribe(data => {  
          this.data.response = data;
          
          
           
                  this._alertCtrl
                      .create({
                          title: 'Atenção ',
                          buttons: [{text: 'OK '}],
                          subTitle: "AQUI FICA A MSG DE RETORNO"
                      }).present();
            

          
      }, error => {
          console.log("Ocorreu algum erro!");
          this._alertCtrl
              .create({
                  title: 'Sem Conexão',
                  buttons: [{text: 'OK'}],
                  subTitle: " Tente em Alguns instantes"
              }).present();
      });
      */

  }

  

}
