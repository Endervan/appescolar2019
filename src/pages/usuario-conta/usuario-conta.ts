import {Component} from '@angular/core';
import {AlertController, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Http} from '@angular/http';
import {Usuario} from '../../domain/usuario/usuario';
import {UsuarioAlteraDadosPage} from "../usuario-altera-dados/usuario-altera-dados";

@Component({
    selector: 'page-usuario-conta',
    templateUrl: 'usuario-conta.html',
})
export class UsuarioContaPage {
    public data;
    public http;
    public usuario: Usuario;


    public url: string;
    public usuarios: Usuario[];



    constructor(public navCtrl: NavController,
                http: Http,
                private _alertCtrl: AlertController,
                private _loadingCtrl: LoadingController,
                public navParams: NavParams) {

        this.data = {};
        this.data.response = '';
        this.http = http;


        this.url = 'http://localhost/escolar/usuario/get_usuario/'+ sessionStorage.getItem('usuarioId')+'/' + sessionStorage.getItem('token');

    }

    ngOnInit(){
        let loader = this._loadingCtrl.create({
            content: 'Buscando Dados. Aguarde...'
        });
        loader.present();
        this.http
            .get(this.url)
            .map( res => res.json())
            .toPromise()
            .then( usuarios => {
                this.usuarios = usuarios;
                loader.dismiss();
            })
            .catch(err =>{
                console.log(err);
                loader.dismiss();
                this._alertCtrl
                    .create({
                        title: 'Falha na conexão',
                        buttons: [{ text: 'OK estou ciente!'}],
                        subTitle: "Não foi possível obter dados. Tente mais tarde."
                    }).present();
            });
    }

    alterar_usuario(usuario){
        this.navCtrl.push(UsuarioAlteraDadosPage, {usuarioSelecionado: usuario});
    }

}
