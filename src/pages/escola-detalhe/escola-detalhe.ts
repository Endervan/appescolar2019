import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';



@Component({
  selector: 'page-escola-detalhe',
  templateUrl: 'escola-detalhe.html',
})
export class EscolaDetalhePage {
  dado:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
   //pegando dados por params
    this.dado = this.navParams.get('detalhes')
  }

}
