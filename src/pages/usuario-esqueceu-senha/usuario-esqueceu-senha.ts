import {Component} from '@angular/core';
import {AlertController, LoadingController, NavController} from 'ionic-angular';
import {Http} from '@angular/http';
import {Usuario} from '../../domain/usuario/usuario';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";


@Component({
    selector: 'page-usuario-esqueceu-senha',
    templateUrl: 'usuario-esqueceu-senha.html',
})
export class UsuarioEsqueceuSenhaPage {
    public data;
    public http;
    usuario:Usuario

    Recupera:FormGroup
    email:AbstractControl

    constructor(public navCtrl: NavController,
                http: Http,
                public formBuilder: FormBuilder,
                public  _loadingCtrl:LoadingController,
                public _alertCtrl:AlertController
    ) {
        this.Recupera = formBuilder.group({
            email: ['',
                Validators.compose(
                    [Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/), Validators.required])],
        });
        this.email = this.Recupera.controls['email']

        this.data = {};
        this.data.response = '';
        this.http = http;
        this.usuario = new Usuario(null, null, null, null, null, null);
    }


    Recupera_senha() {
        var link = 'http://localhost/escolar/usuario/Recupera_senha';
        var data = JSON.stringify({
            email: this.usuario.email

        });
        const loader = this._loadingCtrl.create({
            content: "Processando, por favor aguarde.",
        });
        loader.present();

        // Iniciando a conexão HTTP para cadastro via JSON
        this.http.post(link, data)
            .subscribe(data => {
                this.data.response = data._body;
                var res = this.data.response.split("|");

                console.log(this.data.response)


                if (res[1] == "sucesso") {

                    this._alertCtrl
                        .create({
                            title: 'Atenção',
                            buttons: [{text: 'OK '}],
                            subTitle: " Sua nova senha foi enviada para seu email."
                        }).present();
                        loader.dismiss();

                    }else{
                        this._alertCtrl
                            .create({
                                title: 'Atenção',
                                buttons: [{text: 'OK '}],
                                subTitle: " Email Não Cadastrado."
                            }).present();
                        loader.dismiss();
                    }
            }, error => {
                this._alertCtrl
                    .create({
                        title: 'Sem Conexão',
                        buttons: [{text: 'OK estou ciente!'}],
                        subTitle: " Tente em Alguns instantes"
                    }).present();
            });
        //}
    }

}
