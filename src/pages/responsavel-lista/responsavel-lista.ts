import {Component, ElementRef, ViewChild} from '@angular/core';
import {AlertController, LoadingController, NavController, NavParams, Searchbar, ToastController} from 'ionic-angular';
import {ResponsavelService} from '../../providers/responsavel/responsavel.service';
import {Responsavel} from '../../modelos/responsavel.model';
import {ResponsavelDetalhePage} from '../responsavel-detalhe/responsavel-detalhe';

@Component({
    selector: 'page-responsavel-lista',
    templateUrl: 'responsavel-lista.html',
})
export class ResponsavelListaPage {

    dados: Responsavel[];
    id_responsavel: any = 1;

    @ViewChild('searchbar', {read: ElementRef}) searchbarRef: ElementRef;
    @ViewChild('searchbar') searchbarElement: Searchbar;
    search: boolean = false;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                public toastController: ToastController,
                public  responsavelService: ResponsavelService) {
    }

    ionViewDidLoad() {
        // buscando dados do responsavel logado
        this.getDados()

    }


    public getDados() {
        //  metodo loading
        let loading = this.loadingCtrl.create({
            content: 'Processando.'
        });

        //  exibe loading
        loading.present();

        //  buscando os dados
        this.responsavelService.getDadosId(`${this.id_responsavel}`)
            .then((data) => {
                //  armazena o json na variavel
                this.dados = data.json();
                console.log(this.dados);

                //  oculta loading
                loading.dismiss();
            }).catch((response) => {
            this.alertCtrl
                .create({
                    title: 'Sem Conexão',
                    buttons: [{text: 'OK estou ciente!'}],
                    subTitle: "Tente Mais Tarde."
                }).present();


            //  tira o  loading
            loading.dismiss();



        });

    }

    // filta itens de uma lista dinamicamente
    FiltarItems(ev: any) {
        // set val to the value of the searchbar

        const val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {

            this.dados = this.dados.filter((dado: any) => {
                return (dado.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })

        } else {
            // recupera lista novamente caso barra pesquisa tive vazia
            this.getDados();
        }
    }


    //escondendo barra de pesquisa
    toggleSearch() {
        if (this.search) {
            this.search = false;
        } else {
            this.search = true;
            this.searchbarElement.setFocus();

        }

    }

    async presentToast() {
        const toast = await this.toastController.create({
            message: 'Lista Vazia!! Faça nova pesquisa',
            duration: 2000,
            position: 'middle',
        });
        toast.present();

    }


    // deletar dado do responsavel
    deletar(dado) {
        console.log(dado.id);
        this.responsavelService.deleteData(dado)
            .subscribe(
                data => {
                    const confirm = this.alertCtrl.create({
                        title: 'Atenção',
                        message: 'Escola Excluida com Sucesso',
                        buttons: [
                            {
                                text: 'OK',
                                handler: () => {
                                    this.getDados();
                                }
                            },

                        ]
                    });
                    confirm.present();
                    console.log(data.mensage);
                },
                err => console.log(err)
            );

    }


    // Editar dados responsavel
    editar(dado) {
        console.log(dado);
        console.log(dado.id)
        let prompt = this.alertCtrl.create({
            title: 'Editar Responsavel',
            inputs: [
                {
                    name: 'nome', placeholder: 'nome', value: dado.nome
                },
                {
                    name: 'endereco', placeholder: 'endereco', value: dado.endereco
                }
            ],
            buttons: [
                {
                    text: 'Sair',
                    handler: data => {
                    }
                },
                {
                    text: 'Salvar',
                    handler: data => {
                        let params: any = {
                            id: dado.id,
                            nome: data.nome,
                            endereco: data.endereco,
                        }

                        console.log(data);
                        this.responsavelService.updateData(params)
                            .subscribe(
                                data => {
                                    console.log(data.mensage);
                                    this.getDados();
                                },
                                err => console.log(err)
                            );
                    }
                }
            ]
        });
        prompt.present();
    }



    // passando dados pra outra pagina
    detalhe(dado: any) {
        this.navCtrl.push(ResponsavelDetalhePage, {detalhesResponsavel: dado});
    }



}
