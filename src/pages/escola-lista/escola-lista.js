var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Searchbar, } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { EscolaProvider } from '../../providers/escola/escola';
import 'rxjs/add/observable/fromPromise';
import { Storage } from '@ionic/storage';
var EscolaListaPage = (function () {
    function EscolaListaPage(alertCtrl, navCtrl, navParams, escolaProvider, loadingCtrl, storage) {
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.escolaProvider = escolaProvider;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.searchTerm = '';
        this.search = false;
        this.getDados();
    }
    EscolaListaPage.prototype.getDados = function () {
        var _this = this;
        //  metodo loading
        var loading = this.loadingCtrl.create({
            content: 'Processando, por favor aguarde.'
        });
        //  exibe loading
        loading.present();
        //  buscando os dados
        this.escolaProvider.getDadosId()
            .then(function (data) {
            //  armazena o json na variavel
            _this.dados = data.json();
            console.log(_this.dados);
            //  oculta loading
            loading.dismiss();
        }).catch(function (response) {
            //  exibe loading
            loading.dismiss();
            // this.showAlert();
        });
    };
    /*

    // msg de erro
    showAlert() {
        const alert = this.alertCtrl.create({
            title: 'Aviso',
            subTitle: 'Houve um erro ao executar.',
            buttons: ['OK']
        });
        alert.present();
    }




    // deletar dado
    deletarEscola(dado) {
        console.log(dado.id);
        this.escolaProvider.deleteData(dado)
            .subscribe(
                data => {
                    const confirm = this.alertCtrl.create({
                        title: 'Atenção',
                        message: 'Escola Excluida com Sucesso',
                        buttons: [
                            {
                                text: 'OK',
                                handler: () => {
                                    this.getDados();
                                }
                            },

                        ]
                    });
                    confirm.present();
                    console.log(data.mensage);
                },
                err => console.log(err)
            );

    }


    // Editar dados
    editarEscola(dado) {
        console.log(dado);
        console.log(dado.id)
        let prompt = this.alertCtrl.create({
            title: 'Editar Escola',
            inputs: [
                {
                    name: 'nome', placeholder: 'nome', value: dado.nome
                },
                {
                    name: 'endereco', placeholder: 'endereco', value: dado.endereco
                }
            ],
            buttons: [
                {
                    text: 'Sair',
                    handler: data => {
                    }
                },
                {
                    text: 'Salvar',
                    handler: data => {
                        let params: any = {
                            id: dado.id,
                            nome: data.nome,
                            endereco: data.endereco,
                        }

                        console.log(data);
                        this.escolaProvider.updateData(params)
                            .subscribe(
                                data => {
                                    console.log(data.mensage);
                                    this.getDados();
                                },
                                err => console.log(err)
                            );
                    }
                }
            ]
        });
        prompt.present();
    }

 */
    // passando dados pra outra pagina
    EscolaListaPage.prototype.detalheEscola = function (dado) {
        this.navCtrl.push('EscolaDetalhePage', { detalhes: dado });
    };
    // filta itens de uma lista dinamicamente
    EscolaListaPage.prototype.FiltarItems = function (ev) {
        // set val to the value of the searchbar
        this.getDados();
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.dados = this.dados.filter(function (dado) {
                return (dado.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
        else {
            // recupera lista novamente caso barra pesquisa tive vazia
            this.getDados();
        }
    };
    //escondendo barra de pesquisa
    EscolaListaPage.prototype.toggleSearch = function () {
        if (this.search) {
            this.search = false;
        }
        else {
            this.search = true;
            this.searchbarElement.setFocus();
        }
    };
    __decorate([
        ViewChild('searchbar', { read: ElementRef }),
        __metadata("design:type", ElementRef)
    ], EscolaListaPage.prototype, "searchbarRef", void 0);
    __decorate([
        ViewChild('searchbar'),
        __metadata("design:type", Searchbar)
    ], EscolaListaPage.prototype, "searchbarElement", void 0);
    EscolaListaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-escola-lista',
            templateUrl: 'escola-lista.html',
        }),
        __metadata("design:paramtypes", [AlertController, NavController,
            NavParams, EscolaProvider,
            LoadingController, Storage])
    ], EscolaListaPage);
    return EscolaListaPage;
}());
export { EscolaListaPage };
//# sourceMappingURL=escola-lista.js.map