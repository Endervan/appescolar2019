var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { UsuarioCadastroPage } from "../pages/usuario-cadastro/usuario-cadastro";
import { UsuarioAlteraDadosPage } from "../pages/usuario-altera-dados/usuario-altera-dados";
import { VeiculoListaPage } from '../pages/veiculo-lista/veiculo-lista';
import { UsuarioLoginPage } from "../pages/usuario-login/usuario-login";
import { EscolaCadastroPage } from './../pages/escola-cadastro/escola-cadastro';
import { EscolaListaPage } from '../pages/escola-lista/escola-lista';
import { AlunosPage } from '../pages/alunos/alunos';
var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = EscolaListaPage;
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: HomePage },
            { title: 'Cadrastra-se', component: UsuarioCadastroPage },
            { title: 'Listar Veículos', component: VeiculoListaPage },
            { title: 'Altera Dados', component: UsuarioAlteraDadosPage },
            { title: 'Login', component: UsuarioLoginPage },
            { title: 'Escola Lista', component: EscolaListaPage },
            { title: 'Escola Cadastro', component: EscolaCadastroPage },
            { title: 'Alunos Cadastro', component: AlunosPage }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.push(page.component);
    };
    __decorate([
        ViewChild(Nav),
        __metadata("design:type", Nav)
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Component({
            templateUrl: 'app.html'
        }),
        __metadata("design:paramtypes", [Platform, StatusBar, SplashScreen])
    ], MyApp);
    return MyApp;
}());
export { MyApp };
//# sourceMappingURL=app.component.js.map