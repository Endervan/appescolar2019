import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';


@Injectable()
export class EscolaProvider {

    url: string = 'http://localhost/escolar/escola/';

    constructor(public http: Http) {


    }


    //    busca os dados
    getDados() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get(this.url + 'getDados/', {headers: headers}).toPromise();
    }


//    busca os dados com id
    getDadosId(id:any) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get(this.url + `getDadoId/${id}`, {headers: headers}).toPromise();


    }




    //    cadastro
    postData(parans) {

        console.log('URL = ' + this.url);
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        return this.http.post(this.url + "cadastra", parans, {
            headers: headers,
            method: "POST"
        }).map(
            (res: Response) => {
                return res.json();
            }
        );
    }


    deleteData(id: number) {
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        return this.http.post(this.url + "deletar", id, {
            headers: headers
        }).map(
            (res: Response) => {
                return res.json();
            }
        );
    }


    updateData(data: any) {
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        return this.http.post(this.url + "Update", data, {
            headers: headers,
            method: "POST"
        }).map(
            (res: Response) => {
                return res.json();
            }
        );
    }


}
