import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import {BaseService} from '../base/base.service';

@Injectable()
export class ResponsavelService extends BaseService {


    url: string = 'http://desktop-gl0fvjg/escolar/responsavel/';


    constructor(public http: Http) {
        super();
    }

    //    busca todos os dados
    getDados() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get(this.url + 'getResponsaveis/', {headers: headers}).toPromise();
    }


    //    busca os dados com id usuario lgado
    getDadosId(responsavelId: string) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get(this.url + `getResponsaveis/${responsavelId}`, {headers: headers}).toPromise();
    }



    //    cadastro
    postData(parans) {

        console.log('URL = ' + this.url);
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        return this.http.post(this.url + "cadastra", parans, {
            headers: headers,
            method: "POST"
        }).map(
            (res: Response) => {
                return res.json();
            }
        );
    }


    deleteData(id: number) {
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        return this.http.post(this.url + "deletar", id, {
            headers: headers
        }).map(
            (res: Response) => {
                return res.json();
            }
        );
    }


    updateData(data: any) {
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        return this.http.post(this.url + "Update", data, {
            headers: headers,
            method: "POST"
        }).map(
            (res: Response) => {
                return res.json();
            }
        );
    }


}
