import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EscolaCadastroPage } from './escola-cadastro';

@NgModule({
  declarations: [
    EscolaCadastroPage,
  ],
  imports: [
    IonicPageModule.forChild(EscolaCadastroPage),
  ],
})
export class EscolaCadastroPageModule {}
