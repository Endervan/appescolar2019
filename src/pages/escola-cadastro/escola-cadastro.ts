import {Storage} from '@ionic/storage';
import {FormBuilder, Validators} from '@angular/forms';
import {EscolaProvider} from './../../providers/escola/escola';
import {Component} from '@angular/core';
import {IonicPage, AlertController, NavController, LoadingController} from 'ionic-angular';
import {EscolaListaPage} from '../escola-lista/escola-lista';


@IonicPage()
@Component({
    selector: 'page-escola-cadastro',
    templateUrl: 'escola-cadastro.html',
})
export class EscolaCadastroPage {

    form: any = {};
    id_usuario: any;


    constructor(private storage: Storage, public navCtrl: NavController, public formBuilder: FormBuilder, public service: EscolaProvider, public alertCtrl: AlertController, public loadingCtrl: LoadingController) {

        // this.id_usuario = this.storage.get('idusuario');


        //  campos do fomulario
        this.form = this.formBuilder.group({
            nome: ['', Validators.compose([Validators.required])],
            endereco: ['', Validators.required],
            complemento: [''],
            telefone: [''],
            id_usuario: +this.id_usuario
        });

    }

       //  cadastra no banco
    postDados() {
        //  inicio loading
        const loader = this.loadingCtrl.create({
            content: "Processando, por favor aguarde.",
        });
        loader.present();
        //  envia o post para cadastro
        this.service.postData(this.form.value)
            .subscribe(
                data => {
                    console.log(this.form.value);
                    loader.dismiss(); //  oculta loading
                    this.form.reset();  //  limpa formulario


                    // set novamente id_usuario
                    this.id_usuario = this.storage.get('idusuario');
                    this.ionViewDidEnter()

                    const confirm = this.alertCtrl.create({
                        title: 'Atenção',
                        message: 'Escola cadastra com sucesso. Deseja inserir uma nova escola?',
                        buttons: [
                            {
                                text: 'Sim',
                                handler: () => {

                                }
                            },
                            {
                                text: 'Não',
                                handler: () => {
                                    this.navCtrl.push(EscolaListaPage);
                                }
                            }
                        ]
                    });
                    confirm.present();
                    // this.showAlert('Aviso', data.mensage, 'OK');  //  msg de sucesso

                },
                err => {
                    loader.dismiss(); //  oculta loading
                    this.showAlert('Atenção', err.mensage, 'OK'); //  msg de erro

                    console.log(err);
                }
            );
    }


    //  alert
    showAlert(msgTitle, msgSubTitle, msgButtons) {
        const alert = this.alertCtrl.create({
            title: msgTitle,
            subTitle: msgSubTitle,
            buttons: [msgButtons]
        });
        alert.present();
    }


    // retorna value id_usuario storage
    ionViewDidEnter() {
        return this.storage.get('idusuario').then((val) => {
            return this.id_usuario = val
        });
    }


}
