import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import {UsuarioCadastroPage} from "../pages/usuario-cadastro/usuario-cadastro";
import {UsuarioAlteraDadosPage} from "../pages/usuario-altera-dados/usuario-altera-dados";
import { VeiculoListaPage } from '../pages/veiculo-lista/veiculo-lista';
import {UsuarioLoginPage} from "../pages/usuario-login/usuario-login";
import { EscolaCadastroPage } from './../pages/escola-cadastro/escola-cadastro';
import { EscolaListaPage } from '../pages/escola-lista/escola-lista';
import {AlunosPage} from '../pages/alunos/alunos';
import {ResponsavelListaPage} from '../pages/responsavel-lista/responsavel-lista';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;


  rootPage: any = ResponsavelListaPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
        { title: 'Cadrastra-se', component: UsuarioCadastroPage },
        { title: 'Listar Veículos', component: VeiculoListaPage },
        { title: 'Altera Dados', component: UsuarioAlteraDadosPage },
        { title: 'Login', component: UsuarioLoginPage },
        { title: 'Escola Lista', component: EscolaListaPage },
        { title: 'Escola Cadastro', component: EscolaCadastroPage },
        { title: 'Alunos Cadastro', component: AlunosPage },
        { title: 'Responsavel-lista', component: ResponsavelListaPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push(page.component);
  }



}
