var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { UsuarioAlteraDadosPage } from "../usuario-altera-dados/usuario-altera-dados";
var UsuarioContaPage = (function () {
    function UsuarioContaPage(navCtrl, http, _alertCtrl, _loadingCtrl, navParams) {
        this.navCtrl = navCtrl;
        this._alertCtrl = _alertCtrl;
        this._loadingCtrl = _loadingCtrl;
        this.navParams = navParams;
        this.data = {};
        this.data.response = '';
        this.http = http;
        this.url = 'http://localhost/escolar/usuario/get_usuario/' + sessionStorage.getItem('usuarioId') + '/' + sessionStorage.getItem('token');
    }
    UsuarioContaPage.prototype.ngOnInit = function () {
        var _this = this;
        var loader = this._loadingCtrl.create({
            content: 'Buscando Dados. Aguarde...'
        });
        loader.present();
        this.http
            .get(this.url)
            .map(function (res) { return res.json(); })
            .toPromise()
            .then(function (usuarios) {
            _this.usuarios = usuarios;
            loader.dismiss();
        })
            .catch(function (err) {
            console.log(err);
            loader.dismiss();
            _this._alertCtrl
                .create({
                title: 'Falha na conexão',
                buttons: [{ text: 'OK estou ciente!' }],
                subTitle: "Não foi possível obter dados. Tente mais tarde."
            }).present();
        });
    };
    UsuarioContaPage.prototype.alterar_usuario = function (usuario) {
        this.navCtrl.push(UsuarioAlteraDadosPage, { usuarioSelecionado: usuario });
    };
    UsuarioContaPage = __decorate([
        Component({
            selector: 'page-usuario-conta',
            templateUrl: 'usuario-conta.html',
        }),
        __metadata("design:paramtypes", [NavController,
            Http,
            AlertController,
            LoadingController,
            NavParams])
    ], UsuarioContaPage);
    return UsuarioContaPage;
}());
export { UsuarioContaPage };
//# sourceMappingURL=usuario-conta.js.map