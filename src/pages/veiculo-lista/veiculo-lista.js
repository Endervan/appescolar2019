var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ToastController, LoadingController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { VeiculoProvider } from '../../providers/veiculo/veiculo';
var VeiculoListaPage = (function () {
    function VeiculoListaPage(veiculoProvider, fb, toastCtrl, loadingCtrl) {
        this.veiculoProvider = veiculoProvider;
        this.fb = fb;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.form = this.fb.group({
            cep: ['', Validators.compose([
                    Validators.required
                ])],
        });
    }
    VeiculoListaPage.prototype.ionViewDidLoad = function () {
    };
    VeiculoListaPage.prototype.consultarEnderecoPeloCep = function () {
        var _this = this;
        var aguarde = this.loadingCtrl.create({
            content: "Processando"
        });
        aguarde.present();
        var valueCep = this.form.controls['cep'].value;
        this.veiculoProvider.listarEndereco(valueCep).then(function (data) {
            _this.response = data.json();
            console.log(_this.response);
            aguarde.dismiss();
        }).catch(function (response) {
            aguarde.dismiss();
            _this.toastCtrl.create({
                message: 'Cep não encontrado!',
                duration: 2000,
                position: 'top'
            }).present();
        });
    };
    VeiculoListaPage = __decorate([
        Component({
            selector: 'page-veiculo-lista',
            templateUrl: 'veiculo-lista.html',
        }),
        __metadata("design:paramtypes", [VeiculoProvider, FormBuilder, ToastController, LoadingController])
    ], VeiculoListaPage);
    return VeiculoListaPage;
}());
export { VeiculoListaPage };
//# sourceMappingURL=veiculo-lista.js.map