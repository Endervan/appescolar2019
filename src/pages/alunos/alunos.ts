import {Component} from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController} from 'ionic-angular';
import {FormBuilder, Validators} from '@angular/forms';
import {AlunosProvider} from '../../providers/alunos/alunos';
import {HomePage} from '../home/home';
import {SelectSearchableComponent} from 'ionic-select-searchable';
import {EscolaProvider} from '../../providers/escola/escola';
import {ResponsavelService} from '../../providers/responsavel/responsavel.service';


@IonicPage()
@Component({
    selector: 'page-alunos',
    templateUrl: 'alunos.html',
})
export class AlunosPage {


    form: any;
    response: any;
    dados: any;
    dadosSelect: any;
    dadosEscolar: any;
    dadosSelectEscola: any;


    constructor(public navCtrl: NavController,
                public formBuilder: FormBuilder,
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                private alunosProvider: AlunosProvider,
                private responsavelService: ResponsavelService,
                private escolaProvider: EscolaProvider) {


        //  campos do fomulario
        this.form = this.formBuilder.group({
            nome: ['', Validators.compose([Validators.required])],
            sala: ['', Validators.compose([Validators.minLength(2), Validators.required])],
            sexo: ['', Validators.required],
            turno: ['', Validators.required],
            id_responsavel: ['', Validators.required],
            id_escola: ['', Validators.required],

        });

        // populando dados do select (tabela Responsavel)
        // this.getDados();
        // this.dadosSelect = this.dados;
        //
        // // populando dados do select (tabela escola)
        // this.getDadosEscolar();
        // this.dadosSelectEscola = this.dadosEscolar;


    }




    //  cadastra no banco
    postDados() {

        //  inicio loading
        const loader = this.loadingCtrl.create({
            content: "Processando, por favor aguarde.",
        });
        loader.present();
        //  envia o post para cadastro

        if (this.form != null) {
            this.alunosProvider.postData(this.form.value)
                .subscribe(
                    data => {

                        loader.dismiss(); //  oculta loading
                        this.form.reset();  //  limpa formulario

                        const confirm = this.alertCtrl.create({
                            title: 'Atenção',
                            message: 'Aluno cadastra com sucesso. Deseja inserir uma nova registro?',
                            buttons: [
                                {
                                    text: 'Sim',
                                    handler: () => {

                                    }
                                },
                                {
                                    text: 'Não',
                                    handler: () => {
                                        this.navCtrl.push(HomePage);
                                    }
                                }
                            ]
                        });
                        confirm.present();
                        // this.showAlert('Aviso', data.mensage, 'OK');  //  msg de sucesso

                    },
                    err => {
                        loader.dismiss(); //  oculta loading
                        this.showAlert('Atenção', err.mensage, 'OK'); //  msg de erro

                        console.log(err);
                    });

        }

    }


    //buscando dados tabela responsavel
    // getDados() {
    //     //  buscando os dados
    //     this.responsavelProvider.getDados().then((data) => {
    //         //  armazena o json na variavel
    //         this.dados = data.json();
    //         // console.log(this.dados);
    //     }).catch((response) => {
    //         console.log(response);
    //     });
    //
    //
    // }


    //buscando dados tabela escola
    getDadosEscolar() {
        //  buscando os dados
        this.escolaProvider.getDados().then((data) => {
            //  armazena o json na variavel
            this.dadosEscolar = data.json();
            // console.log(this.dados);
        }).catch((response) => {
            console.log(response);
        });


    }


    //  alert
    showAlert(msgTitle, msgSubTitle, msgButtons) {
        const alert = this.alertCtrl.create({
            title: msgTitle,
            subTitle: msgSubTitle,
            buttons: [msgButtons]
        });
        alert.present();
    }


    // filtra conteudo Select
    portChange(event: {
        component: SelectSearchableComponent,
        value: any;
    }) {
        console.log('array select:', event.value);
    }



}

