var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Usuario } from '../../domain/usuario/usuario';
import { FormBuilder, Validators } from '@angular/forms';
import { HomePage } from '../home/home';
var UsuarioCadastroPage = (function () {
    function UsuarioCadastroPage(navCtrl, http, formBuilder, _alertCtrl, _loadingCtrl) {
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this._alertCtrl = _alertCtrl;
        this._loadingCtrl = _loadingCtrl;
        this.Cadastro = formBuilder.group({
            nome: ['', Validators.compose([Validators.maxLength(30), Validators.required])],
            email: ['',
                Validators.compose([Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/), Validators.required])],
            password: ['',
                Validators.compose([Validators.minLength(3), /*Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$'),*/ Validators.required])],
            telefone: ['', Validators.compose([Validators.minLength(14), Validators.required])],
        });
        this.nome = this.Cadastro.controls['nome'];
        this.email = this.Cadastro.controls['email'];
        this.password = this.Cadastro.controls['password'];
        this.telefone = this.Cadastro.controls['telefone'];
        this.data = {};
        this.data.response = '';
        this.http = http;
        this.usuario = new Usuario(null, null, null, null, null, null);
    }
    UsuarioCadastroPage.prototype.Cadastrar = function () {
        var _this = this;
        var link = 'http://localhost/escolar/usuario/cadastrar_usuario_ionic';
        var data = JSON.stringify({
            nome: this.usuario.nome,
            email: this.usuario.email,
            telefone: this.usuario.telefone,
            password: this.usuario.password
        });
        var loader = this._loadingCtrl.create({
            content: "Processando, por favor aguarde.",
        });
        loader.present();
        console.log(data);
        loader.dismiss();
        // Iniciando a conexão HTTP para cadastro via JSON
        this.http.post(link, data)
            .subscribe(function (data) {
            _this.data.response = data._body;
            var res = _this.data.response.split("|");
            console.log(_this.data.response);
            if (res[0] == "sucesso") {
                var loader_1 = _this._loadingCtrl.create({
                    content: ' Cadastro Efetuado com sucesso '
                });
                loader_1.present();
                loader_1.dismiss();
                _this.navCtrl.push(HomePage);
            }
            else {
                console.log(res[0]);
                if (res[0] == 1) {
                    _this._alertCtrl
                        .create({
                        title: 'Atenção ',
                        buttons: [{ text: 'OK ' }],
                        subTitle: " Esse email já está sendo utilizado."
                    }).present();
                    loader.dismiss();
                }
                else if (res[0] == 0) {
                    _this._alertCtrl
                        .create({
                        title: 'Atenção',
                        buttons: [{ text: 'OK ' }],
                        subTitle: " Esse telefone já está sendo utilizado."
                    }).present();
                    loader.dismiss();
                }
            }
        }, function (error) {
            console.log("Ocorreu algum erro!");
            _this._alertCtrl
                .create({
                title: 'Sem Conexão',
                buttons: [{ text: 'OK estou ciente!' }],
                subTitle: " Tente em Alguns instantes"
            }).present();
        });
        //}
    };
    UsuarioCadastroPage = __decorate([
        Component({
            selector: 'page-usuario-cadastro',
            templateUrl: 'usuario-cadastro.html',
        }),
        __metadata("design:paramtypes", [NavController,
            Http,
            FormBuilder,
            AlertController,
            LoadingController])
    ], UsuarioCadastroPage);
    return UsuarioCadastroPage;
}());
export { UsuarioCadastroPage };
//# sourceMappingURL=usuario-cadastro.js.map