export class Usuario{

    constructor(
        public id: string,
        public nome: string,
        public email: string,
        public password: string,
        public telefone:number,
        public status: string ){}
}