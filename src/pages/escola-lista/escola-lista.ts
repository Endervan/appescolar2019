import {Component, ElementRef, ViewChild} from '@angular/core';
import {
    IonicPage, NavController,
    NavParams, LoadingController,
    AlertController, Searchbar,
} from 'ionic-angular';

import 'rxjs/add/operator/map';
import {EscolaProvider} from '../../providers/escola/escola';
import 'rxjs/add/observable/fromPromise';
import {Storage} from '@ionic/storage';
import {EscolaDetalhePage} from '../escola-detalhe/escola-detalhe';


@IonicPage()
@Component({
    selector: 'page-escola-lista',
    templateUrl: 'escola-lista.html',
})
export class EscolaListaPage {
    response: any;
    searchTerm: string = '';
    dados: Array<any>;

    id_usuario: any;


    @ViewChild('searchbar', {read: ElementRef}) searchbarRef: ElementRef;
    @ViewChild('searchbar') searchbarElement: Searchbar;
    search: boolean = false;


    constructor(public alertCtrl: AlertController, public navCtrl: NavController,
                public navParams: NavParams, private escolaProvider: EscolaProvider,
                public loadingCtrl: LoadingController, private storage: Storage) {

        if (this.id_usuario != '') {
            this.storage.get('idusuario')
                .then((idusuario: any) => {
                    idusuario

                    // pega dados com id que ta storage
                    this.id_usuario = idusuario;
                    console.log('Recebendo idusuario (storage)=>' + this.id_usuario);

                        this.getDados();

                });
        }

    }






     public  getDados() {
        //  metodo loading
        let loading = this.loadingCtrl.create({
            content: 'Processando, por favor aguarde.'
        });

        //  exibe loading
        loading.present();

        //  buscando os dados
        this.escolaProvider.getDadosId(`${this.id_usuario}`)
            .then((data) => {
                //  armazena o json na variavel
                this.dados = data.json();
                console.log(this.dados);

                //  oculta loading
                loading.dismiss();
            }).catch((response) => {

            //  exibe loading
            loading.dismiss();

            // this.showAlert();

        });

    }




    // msg de erro
    showAlert() {
        const alert = this.alertCtrl.create({
            title: 'Aviso',
            subTitle: 'Houve um erro ao executar.',
            buttons: ['OK']
        });
        alert.present();
    }




    // deletar dado
    deletarEscola(dado) {
        console.log(dado.id);
        this.escolaProvider.deleteData(dado)
            .subscribe(
                data => {
                    const confirm = this.alertCtrl.create({
                        title: 'Atenção',
                        message: 'Escola Excluida com Sucesso',
                        buttons: [
                            {
                                text: 'OK',
                                handler: () => {
                                    this.getDados();
                                }
                            },

                        ]
                    });
                    confirm.present();
                    console.log(data.mensage);
                },
                err => console.log(err)
            );

    }


    // Editar dados
    editarEscola(dado) {
        console.log(dado);
        console.log(dado.id)
        let prompt = this.alertCtrl.create({
            title: 'Editar Escola',
            inputs: [
                {
                    name: 'nome', placeholder: 'nome', value: dado.nome
                },
                {
                    name: 'endereco', placeholder: 'endereco', value: dado.endereco
                }
            ],
            buttons: [
                {
                    text: 'Sair',
                    handler: data => {
                    }
                },
                {
                    text: 'Salvar',
                    handler: data => {
                        let params: any = {
                            id: dado.id,
                            nome: data.nome,
                            endereco: data.endereco,
                        }

                        console.log(data);
                        this.escolaProvider.updateData(params)
                            .subscribe(
                                data => {
                                    console.log(data.mensage);
                                    this.getDados();
                                },
                                err => console.log(err)
                            );
                    }
                }
            ]
        });
        prompt.present();
    }



    // passando dados pra outra pagina
    detalheEscola(dado: any) {
        this.navCtrl.push(EscolaDetalhePage, {detalhes: dado});
    }


    // filta itens de uma lista dinamicamente
    FiltarItems(ev: any) {
        // set val to the value of the searchbar

        const val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.dados = this.dados.filter((dado: any) => {
                return (dado.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        } else {
            // recupera lista novamente caso barra pesquisa tive vazia
            this.getDados();
        }
    }

    //escondendo barra de pesquisa
    toggleSearch() {
        if (this.search) {
            this.search = false;
        } else {
            this.search = true;
            this.searchbarElement.setFocus();

        }

    }


    /*
    buscarEndereco(){
      let aguarde = this.loadingCtrl.create({
          content: "Processando, aguarde"
      });

      aguarde.present();

      this.escolaProvider.listarEndereco(72610417);

      aguarde.dismiss();

    }


    consultarEnderecoPeloCep() {
      let aguarde = this.loadingCtrl.create({
        content: "Processando"
      });

      aguarde.present();

      let valueCep = 72610417;
      this.escolaProvider.listarEndereco(valueCep).then((data) => {
        this.response = data.json();

        console.log(this.response);

        aguarde.dismiss();

      }).catch((response) => {
        aguarde.dismiss();
        this.toastCtrl.create({
          message : 'Cep não encontrado!',
          duration: 2000,
          position : 'top'
        }).present();
      });
    }

    */


}
