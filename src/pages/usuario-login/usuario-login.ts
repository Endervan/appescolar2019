import {Component} from '@angular/core';
import {AlertController, LoadingController, NavController, ToastController} from 'ionic-angular';
import {Http} from '@angular/http';
import {Usuario} from '../../domain/usuario/usuario';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UsuarioCadastroPage} from "../usuario-cadastro/usuario-cadastro";
import {UsuarioEsqueceuSenhaPage} from "../usuario-esqueceu-senha/usuario-esqueceu-senha";
import { Storage } from '@ionic/storage';
import {HomePage} from '../home/home';

@Component({
    selector: 'page-usuario-login',
    templateUrl: 'usuario-login.html',
})
export class UsuarioLoginPage {
    public data;
    public http;
    public usuario: Usuario;
    Logando: FormGroup;
    email: AbstractControl;
    password: AbstractControl;

    constructor(public navCtrl: NavController,
                http: Http,
                public formBuilder: FormBuilder,
                private _alertCtrl: AlertController,
                private _loadingCtrl: LoadingController,
                private storage: Storage,
                private _toastCtrl:ToastController) {
        this.Logando = formBuilder.group({
            email: ['',
                Validators.compose(
                    [Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/), Validators.required])],
            password: ['', Validators.required],
        });
        this.email = this.Logando.controls['email']
        this.password = this.Logando.controls['password']

        this.data = {};
        this.data.response = '';
        this.http = http;
        this.usuario = new Usuario(null, null, null, null, null, null);
    }

    //buscando BD os dados pra login
    Login() {
        var link = 'http://localhost/escolar/usuario/login_ionic';
        var data = JSON.stringify({email: this.usuario.email, password: this.usuario.password})
        const loader = this._loadingCtrl.create({
            content: "Processando, por favor aguarde.",
        });
        loader.present();

        this.http.post(link, data)
            .subscribe(data => {
                this.data.response = data._body;
                var res = this.data.response.split("|");

                console.log(this.data.response);
                if (res[1] == "sucesso") {
                   sessionStorage.setItem("usuarioId", res[0]);
                   sessionStorage.setItem("usuarioLogado", this.usuario.email);
                   sessionStorage.setItem("logado", "sim");
                   sessionStorage.setItem("token", res[3]);


                    //  testando o IonicStorageModule 
                    this.storage.set('idusuario',  res[0]);
                    this.storage.set('usuarioLogado', this.usuario.email);
                    this.storage.set('logado', 'sim');
                    this.storage.set('token', res[3]);


                    loader.dismiss();

                    this.navCtrl.setRoot(HomePage);

                } else{
                    let toast = this._toastCtrl.create({
                        message: 'Email ou Senha Invalídos',
                        duration: 3000,
                        position: 'top'
                    });
                    toast.onDidDismiss(() => {
                        console.log('Dismissed toast');
                    });
                    toast.present();

                    loader.dismiss();
                }
            }, error => {
                console.log("Ocorreu algum erro!");
                this._alertCtrl
                    .create({
                        title: 'Sem Conexão',
                        buttons: [{text: 'OK estou ciente!'}],
                        subTitle: "Tente Mais Tarde."
                    }).present();
            });
        loader.dismiss();
    }

    IrCadastro(){
        this.navCtrl.push(UsuarioCadastroPage);

    }

    EsqueceuSenha(){
        this.navCtrl.push(UsuarioEsqueceuSenhaPage);

    }

}
