import {Component} from '@angular/core';
import {AlertController, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Http} from '@angular/http';
import {Usuario} from '../../domain/usuario/usuario';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HomePage} from "../home/home";

@Component({
    selector: 'page-usuario-altera-dados',
    templateUrl: 'usuario-altera-dados.html',
})
export class UsuarioAlteraDadosPage {
    public data;
    public http;
    public usuario: Usuario;
    Cadastro: FormGroup;
    nome: AbstractControl;
    email: AbstractControl;
    telefone: AbstractControl;

    public url: string;
    public usuarios: Usuario;
    public flagUpdate: boolean;



    constructor(public navCtrl: NavController,
                http: Http,
                public formBuilder: FormBuilder,
                private _alertCtrl: AlertController,
                private _loadingCtrl: LoadingController,
                public navParams: NavParams) {

        this.Cadastro = formBuilder.group({
            nome: ['', Validators.compose([Validators.maxLength(30), Validators.required])],
            email: ['', Validators.compose([Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/), Validators.required])],
            telefone: ['',Validators.compose([Validators.minLength(14), Validators.required])],
        });

        this.nome = this.Cadastro.controls['nome']
        this.email = this.Cadastro.controls['email']
        this.telefone = this.Cadastro.controls['telefone']

        this.data = {};
        this.data.response = '';
        this.http = http;

        if (this.navParams.get('usuarioSelecionado')) {
            this.flagUpdate = true;
            this.usuario = this.navParams.get('usuarioSelecionado');
        } else {
            this.flagUpdate = false;
            this.usuario = new Usuario(null, null, null, null, null,null);
        }

        this.url = 'http://localhost/escolar/usuario/get_usuario/'+ sessionStorage.getItem('usuarioId')+'/' + sessionStorage.getItem('token');

    }

    ngOnInit(){
        let loader = this._loadingCtrl.create({
            content: 'Buscando Dados. Aguarde...'
        });
        loader.present();
        this.http
            .get(this.url)
            .map( res => res.json())
            .toPromise()
            .then( usuarios => {
                this.usuarios = usuarios;
                loader.dismiss();
            })
            .catch(err =>{
                console.log(err);
                loader.dismiss();
                this._alertCtrl
                    .create({
                        title: 'Falha na conexão',
                        buttons: [{ text: 'OK estou ciente!'}],
                        subTitle: "Não foi possível obter dados. Tente mais tarde."
                    }).present();
            });
    }


    Cadastrar() {
        var link = 'http://localhost/escolar/usuario/alterar/';
        var data = JSON.stringify({
            id: this.usuario.id,
            nome: this.usuario.nome,
            email: this.usuario.email,
            telefone: this.usuario.telefone,
             flag_update: this.flagUpdate
        });

        const loader = this._loadingCtrl.create({
            content: "Processando, por favor aguarde.",
        });
        loader.present();

        // Iniciando a conexão HTTP para cadastro via JSON
        this.http.post(link, data)
            .subscribe(data => {
                this.data.response = data._body;
                var res = this.data.response.split("|");

                console.log(this.data.response);

                if (res[0] == "sucesso") {
                    let loader = this._loadingCtrl.create({
                        content: ' Cadastro Efetuado com sucesso ',
                    });
                    this.navCtrl.push(HomePage);

                    loader.dismiss();
                } else {
                    console.log(res[0])

                    if (res[0]==1){
                        this._alertCtrl
                            .create({
                                title: 'Atenção ',
                                buttons: [{text: 'OK '}],
                                subTitle: " Esse email já está sendo utilizado."
                            }).present();

                        loader.dismiss();

                    }else if(res[0]==0){
                        this._alertCtrl
                            .create({
                                title: 'Atenção',
                                buttons: [{text: 'OK '}],
                                subTitle: " Esse telefone já está sendo utilizado."
                            }).present();

                        loader.dismiss();

                    }

                }
            }, error => {
                console.log("Ocorreu algum erro!");
                loader.dismiss();
                this._alertCtrl
                    .create({
                        title: 'Sem Conexão',
                        buttons: [{text: 'OK !'}],
                        subTitle: " Tente em Alguns instantes"
                    }).present();
            });
        //}
    }


}
