import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class VeiculoProvider {

  id : number;

  constructor(public http: Http ) {
    
  }

  listarEndereco(cep : number){
  //  let url = "https://viacep.com.br/ws/" + cep + "/json/";
    let url = "http://localhost/Escolar/veiculo/get_dados/1";
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    

    return this.http.post(url, { headers: headers }).toPromise();
  }


}

    